/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestioproblemes;

import net.infobosccoma.coloredconsole.Basics;
import static net.infobosccoma.coloredconsole.Basics.escriureText;
import static org.fusesource.jansi.Ansi.Color.BLACK;
import static org.fusesource.jansi.Ansi.Color.WHITE;

/**
 * Estructura tupla per emmagatzemar les dades d'un problema
 */
class Problema {

    int id;
    String nom, tipus, creador;
}

/**
 * Estructura tupla per emmagatzemar les dades varis problemes
 */
class LlistaProblemes {

    Problema[] dades;
    int nDades;
}

/**
 * Modul amb operacions per gestionar dades de problemes
 * 
 */
public class ModProblema {

    /**
     * Funció que llegeix un problema demanant les dades a l'usuari
     *
     * @return Una tupla de tipus Problema amb les dades plenes.
     */
    public Problema llegir() {
        Problema p = new Problema();

        escriureText(WHITE, BLACK, "NOM:", 5, 6);
        escriureText(BLACK, WHITE, "                       ", 14, 6);

        escriureText(WHITE, BLACK, "TIPUS:", 5, 7);
        escriureText(BLACK, WHITE, "                       ", 14, 7);

        escriureText(WHITE, BLACK, "CREADOR:", 5, 8);
        escriureText(BLACK, WHITE, "                       ", 14, 8);

        p.id = Basics.llegirEnter(14, 5);
        p.nom = Basics.llegirText(14, 6);
        p.tipus = Basics.llegirText(14, 7);
        p.creador = Basics.llegirText(14, 8);

        return p;
    }

    /**
     * Mostra per pantalla les dades d'un problema
     *
     * @param p El problema del qual es volen mostrar les dades
     */
    public void escriure(Problema p) {
        Basics.escriureText(String.format("%2d %-15s %-13s %-16s\n", p.id, p.nom, p.tipus, p.creador));
    }

    /**
     * Afegeix un problema a una llista de problemes.
     *
     * @param llista La llista dels problemes
     * @param p El problema que es vol afegir
     */
    public void afegir(LlistaProblemes llista, Problema p) {
        llista.dades[llista.nDades++] = p;
    }

    /**
     * Mostra tots els problemes d'una llista de problemes
     *
     * @param llista La llista que conté els problemes
     */
    public void llistar(LlistaProblemes llista) {
        int y = 7;
        for (int i = 0; i < llista.nDades; i++) {
            Basics.situarCursor(10, y++);
            escriure(llista.dades[i]);
        }
    }
}
