package gestioproblemes;

import net.infobosccoma.coloredconsole.Basics;
import static net.infobosccoma.coloredconsole.Basics.*;
import net.infobosccoma.coloredconsole.Menu;
import net.infobosccoma.coloredconsole.TipusMissatge;
import static org.fusesource.jansi.Ansi.Color.BLACK;
import static org.fusesource.jansi.Ansi.Color.CYAN;
import static org.fusesource.jansi.Ansi.Color.WHITE;

/**
 * Programa que permet afegir enunciats de problemes de programació a una llista
 * de problemes.
 * 
 * @author marc
 */
public class GestioProblemes {

    static final int MAX = 1000;
    static ModProblema modulProblemes;

    public static void main(String[] args) {
        int opcio;
        LlistaProblemes llista = new LlistaProblemes();
        llista.dades = new Problema[MAX];
        Menu menu = new Menu();

        // carregar a memòria el mòdul amb els subprogrames
        modulProblemes = new ModProblema();

        menu.afegirOpcio("Afegir un problema", 1);
        menu.afegirOpcio("Llistar problemes", 2);
        menu.afegirOpcio("Sortir", 3);

        do {
            esborrarPantalla();
            menu.mostrar();
            switch (opcio = menu.llegirOpcio(30, 16)) {
                case 1:
                    afegirProblema(llista);
                    break;
                case 2:
                    llistar(llista);
                    break;
                case 3:
                    break;
            }
        } while (opcio != 3);
        pausa();
    }

    /**
     * Subprograma que controla si la llista no es plena i, en aquest cas, 
     * permet afegir un nou problema
     * 
     * @param llista La llista on es vol afegir un nou problema
     */
    static void afegirProblema(LlistaProblemes llista) {
        esborrarPantalla();
        escriureTitol(BLACK, CYAN, "A F E G I R  P R O B L E M A", 1);
        escriureText(WHITE, BLACK, "ID:", 5, 5);
        escriureText(BLACK, WHITE, "   ", 14, 5);

        if (llista.nDades == MAX) {
            Basics.escriureMissatge(TipusMissatge.ERROR, "Llista plena!");
        } else {
            modulProblemes.afegir(llista, modulProblemes.llegir());
        }

        pausa();
    }

    /**
     * Subprograma que llist el contingut d'una llista de problemes si aquesta
     * no és buida.
     * 
     * @param llista La llista que conté els problemes a llistar.
     */
    private static void llistar(LlistaProblemes llista) {
        esborrarPantalla();
        escriureTitol(BLACK, CYAN, "L L I S T A R  P R O B L E M E S", 1);

        if (llista.nDades == 0) {
            Basics.escriureMissatge(TipusMissatge.AVIS, "Llista buida!");
        } else {
            escriureText(BLACK, WHITE, "ID NOM             TIPUS         CREADOR         ", 10, 5);
            escriureText(BLACK, WHITE, "=================================================", 10, 6);
            modulProblemes.llistar(llista);
        }
        pausa();
    }
}
